<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
    <link rel="stylesheet" href="styleRegistro.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row">
        <div class="col-sm">
        <div class="registro">  
        <div class="centrar">
            <br><br><img src="registro.png"></img>   
            <h3>Información Personal</h3>
            <form action="index2.php" method="POST" > 
                <div class="form-group">
                    <label for = "Nombre"></label>
                    <input id="nom" type="text" name= "nombre"placeholder="Digite su nombre" required><br>
                    <label for = "Apellido"></label>
                    <br><input id="apell" type="text" name="apellido" placeholder="Digite sus apellidos" required><br>
                    <label for = "Telefono"></label>
                    <br><input id="tel" type="text" name="telefono" placeholder="Digite su telefono" required><br>
                    <label for = "Correo"></label>
                    <br><input id="email" type="text" name="correo" placeholder="Digite su correo" required><br>
                    <label for = "direccion"></label>
                    <br><input id="dir" type="text" name="direccion" placeholder="Digite su dirección" required><br>                    
                    <label for = "Contrasena"></label>
                    <br><input id="contra" type="password" name="contrasena" placeholder="Digite su Contraseña" required><br>
                    <br><input type="radio" name="tipouso"  value="admin" required <?php if (isset($tipousu) && $tipousu=="admin") echo "checked";?>> Administrador
                    <input type="radio" name="tipouso" value="cliente" required <?php if (isset($tipousu) && $tipousu=="cliente") echo "checked";?>> Cliente<br><br>
                    <button name="guardar" class="Registro btn btn-primary">Guardar</button>
                </div>                               
            </form>
            <input type="reset" class="Registro btn btn-primary" value="Restaurar formulario">

        </div>        
        </div>
        </div>
        </div>
    </div>
</body>
</html>

<?php
    if(isset($_POST['guardar'])){
        $name = $_POST['nombre'];
        $ape = $_POST['apellido'];
        $telefono = $_POST['telefono'];
        $correo = $_POST['correo'];
        $direccion = $_POST['direccion'];
        $contrasena = $_POST['contrasena'];
        $tipousu = $_POST['tipouso']; 
        $tipo = 0;
        if ($tipousu == "admin") {
            $tipo = 1;
        }else{
            $tipo = 2;
        }            
        $sql = "INSERT INTO usuario(nombre,apellido,tel,correo,direccion,contrasena,tipousu)
        VALUES ('".$name."','".$ape."','".$telefono."','".$correo."','".$direccion."','".$contrasena."','".$tipo."')";
        //echo $sql; die;
        $con = mysqli_connect("localhost", "root", "", "proyecto")   or die("conexion exitosa!");
        $ejecutar = mysqli_query($con, $sql);
        if ($ejecutar) {
            echo "<h3>Insertado Correctamente</h3>";
        }
    }
?>