<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
        <div class="row">
          <div class="col-sm">
    <!--En esta area se encuentra una imagen de fondo el iniciar sesión, pide datos
    de usuario y contraseña, en caso de no tener cuentra se encuentra el button de registro-->
    <div class="login col-sm-12 col-sm-6"> 
    <div class="centrar">
        <br><br><img src="user_icon-icons.com_66546.png"></img>    
            <h3>Datos de cuenta</h3>
            <form action="/login.php" method="POST">
                <div class="form-group">
                    <label for = "Usuario"></label>
                    <input id="email" type="text" name="correo" placeholder="Digite su correo" required><br>
                    <br><label for = "Contraseña"></label>
                    <input id="contra" type="password" name="contrasena" placeholder="Digite su contraseña" required><br><br>
                    <button type="submit" class="Inicio btn btn-primary">Iniciar sesión</button> 
                </div>                           
            </form>            
            <a href="index2.php"><button type="button" class="btn btn-success">Crear cuenta</button></a>   
            
    </div>
    </div>
</div>
</div>
</div>
</body>
</html>