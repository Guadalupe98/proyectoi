
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrador</title>
    <link rel="stylesheet" href="styleInicioAdmin.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm">
            <header>
    <nav class="navegacion">
      <ul class="menu">
        <li><a href="#">Categorías</a>
          <ul class="submenu">
            <li><a href="verCategorias.php">Ver Categorías</a></li>
            <li><a href="agregarCategoria.php">Agregar Categoría</a></li>
            <li><a href="editarCategoria.php">Editar Categoría</a></li>
            <li><a href="eliminarCategoria.php">Eliminar Categoría</a></li>
          </ul>
        </li>
                <li><a href="#">Productos</a>
                <ul class="submenu">
            <li><a href="verProductos.php">Ver Productos</a></li>
            <li><a href="agregarProducto.php">Agregar Producto</a></li>
            <li><a href="editarProducto.php">Editar Producto</a></li>
            <li><a href="eliminarProducto.php">Eliminar Producto</a></li>
          </ul>
            
            
            
                </li>
            <li>
                <a href="index.php" name="salir">Cerrar Sesión <?php
                if (isset($salir)) {
                    session_start();
                    session_destroy();
                    header("Location: index.php");
                }                
                ?></a></li>
                
            </ul>

    </nav>
    <style type="text/css">
    h4 {text-align: center}
    </style>    
    <br><br><h4>
        <?php 
            require('conexion.php');
            $conn = getConnection();
            $sql = "SELECT COUNT(*) FROM usuario";
            $result = $conn->query($sql);
  
            if ($conn->connect_errno) {
                $conn->close();
                return [];
            }
            $conn->close();
            $v = $result->fetch_array();
            printf ("Cantida de clientes: %s\n",$v["0"]);

        ?></H4><br><br>
    <style type="text/css">
    h4 {text-align: center}
    </style>    
    <br><h4>
        <?php 
            $sql = "SELECT SUM(cantprod) FROM carrito";
            $con = mysqli_connect("localhost", "root", "", "proyecto")   or die("conexion exitosa!");
            $ejecutar = mysqli_query($con, $sql);
            $v = $ejecutar->fetch_array();
            printf ("Cantidad de productos vendidos: %s\n",$v["0"]);

        ?></H4><br><br>
    <style type="text/css">
    h4 {text-align: center}
    </style>    
    <br><h4>
        <?php 
            $sql = "SELECT SUM(total) FROM orden";
            $con = mysqli_connect("localhost", "root", "", "proyecto")   or die("conexion exitosa!");
            $ejecutar = mysqli_query($con, $sql);
            $v = $ejecutar->fetch_array();
            printf ("Monto total de ventas: ₡%s\n",$v["0"]);

        ?></H4>
    </header>           
            </div>
        </div>
    </div>
</body>
</html>


