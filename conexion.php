<?php
/**
 *  Gets a new mysql connection
 */
function getConnection() {
    $connection = new mysqli("localhost", "root", "", "proyecto");
    if ($connection->connect_errno) {
      printf("Connect failed: %s\n", $connection->connect_error);
      die;
    }
    return $connection;
  }
?>