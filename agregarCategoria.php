<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VerCtegoria</title>
    <link rel="stylesheet" href="styleInicioAdmin.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
        <div class="row">
            <div class="col-sm">
            <header>
    <nav class="navegacion">
      <ul class="menu">
      <li><a href="admi.php">Pagina Inicial</a>
        </li>
        <li><a href="#">Categorías</a>
          <ul class="submenu">
            <li><a href="verCategorias.php">Ver Categorías</a></li>
            <li><a href="agregarCategoria.php">Agregar Categoría</a></li>
            <li><a href="editarCategoria.php">Editar Categoría</a></li>
            <li><a href="eliminarCategoria.php">Eliminar Categoría</a></li>
          </ul>
        </li>
                <li><a href="#">Productos</a>
                <ul class="submenu">
            <li><a href="verProductos.php">Ver Productos</a></li>
            <li><a href="agregarProducto.php">Agregar Producto</a></li>
            <li><a href="editarProducto.php">Editar Producto</a></li>
            <li><a href="eliminarProducto.php">Eliminar Producto</a></li>
          </ul>
            
            
            
                </li>
            <li>
                <a href="index.php" name="salir">Cerrar Sesión <?php
                if (isset($salir)) {
                    session_start();
                    session_destroy();
                    header("Location: index.php");
                }                
                ?></a></li>
                
            </ul>

    </nav>
    </header>  
             
            </div>
        </div>
    </div>
            <form action="agregarCategoria.php" method="POST" > 
                <div class="form-group" style = "text-align: center">
                    <br><h3>Nombre de la Categoría</h3>
                    <label for = "Nombre"></label>
                    <input id="nom" type="text" name= "nombre"placeholder="Digite el nombre" required><br><br>
                    <button name="guardar" class="Registro btn btn-primary">Guardar</button>
                </div>                               
            </form>

</body>
</html>

<?php
    if(isset($_POST['guardar'])){
        $name = $_POST['nombre'];  
        $sql = "INSERT INTO categoria(nombre) VALUES ('".$name."')";
        //echo $sql; die;
        $con = mysqli_connect("localhost", "root", "", "proyecto")   or die("conexion exitosa!");
        $ejecutar = mysqli_query($con, $sql);
        if ($ejecutar) {
            echo "<h3 >Insertado Correctamente</h3>";
        }
    }
?>