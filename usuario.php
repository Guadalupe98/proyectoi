<?php
class Usuario{
    public $id;
    public $nombre;
    public $apellido;
    public $tel;
    public $correo;
    public $direccion;
    public $contrasena;
    public $tipousu;
    public $logueado;

    function __construct() {
        
       }
    // from PHP 5.0 and above you can do a constructor like this:
    function __construct($id, $nombre, $apellido,$tel,$correo,$direccion,$contrasena,$tipousu,$logueado) {
     $this->id = $id;
     $this->nombre = $nombre;
     $this->apellido = $apellido;
     $this->tel = $tel;
     $this->correo = $correo;
     $this->direccion = $direccion;
     $this->contrasena = $contrasena;
     $this->tipousu = $tipousu;
     $this->logueado = $logueado;
    }
    function set_logueado($logueado) {
        $this->logueado = $logueado;
      }
      function get_logueado() {
        return $this->logueado;
      }
    function to_string() {
       return “{$this->id}:{$this->nombre}:{$this->apellido}:{$this->tel}:{$this->correo}:{$this->direccion}:{$this->contrasena}:{$this->tipousu}:{$this->logueado}”;
    }
  }
  
?>