<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VerCtegoria</title>
    <link rel="stylesheet" href="styleInicioAdmin.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
        <div class="row">
            <div class="col-sm">
            <header>
    <nav class="navegacion">
      <ul class="menu">
      <li><a href="admi.php">Pagina Inicial</a>
        </li>
        <li><a href="#">Categorías</a>
          <ul class="submenu">
            <li><a href="verCategorias.php">Ver Categorías</a></li>
            <li><a href="agregarCategoria.php">Agregar Categoría</a></li>
            <li><a href="editarCategoria.php">Editar Categoría</a></li>
            <li><a href="eliminarCategoria.php">Eliminar Categoría</a></li>
          </ul>
        </li>
                <li><a href="#">Productos</a>
                <ul class="submenu">
            <li><a href="verProductos.php">Ver Productos</a></li>
            <li><a href="agregarProducto.php">Agregar Producto</a></li>
            <li><a href="editarProducto.php">Editar Producto</a></li>
            <li><a href="eliminarProducto.php">Eliminar Producto</a></li>
          </ul>
            
            
            
                </li>
            <li>
                <a href="index.php" name="salir">Cerrar Sesión <?php
                if (isset($salir)) {
                    session_start();
                    session_destroy();
                    header("Location: index.php");
                }                
                ?></a></li>
                
            </ul>

    </nav>
    </header>      
    <br><br><div style="text-align:center;">       
    <table id="tablaCat" border="1"  width="1110" height="100" >
            <tr>
                <td  bgcolor="rosybrown">SKU</td>
                <td bgcolor="rosybrown">NOMBRE</td>
                <td bgcolor="rosybrown">DESCRIPCIÓN</td>
                <td bgcolor="rosybrown">IMAGEN</td>
                <td bgcolor="rosybrown">PRECIO</td>
                <td bgcolor="rosybrown"></td>          
            </tr>
        <?php
          include('producto.php');
          $pro = new Producto();                    
          $sql = "SELECT * FROM producto";
          $con = mysqli_connect("localhost", "root", "", "proyecto")   or die("conexion exitosa!");
          $result = mysqli_query($con,$sql);
          //echo $result; die;
          while($mostrar=mysqli_fetch_array($result)){ ?>
            <tr>
                <td bgcolor="rosybrown"><?php $pro->SKU = $mostrar['SKU']; echo $pro->SKU; ?></td>
                <td bgcolor="rosybrown"><?php $pro->nombre = $mostrar['nombre']; echo $pro->nombre;?></td>
                <td bgcolor="rosybrown"><?php $pro->descripcion = $mostrar['descripcion']; echo $pro->descripcion;?></td>
                <td bgcolor="rosybrown"><img height = "70px" src="data:image/jpg;base64,<?php echo base64_encode($mostrar['imagen']) ;?>"/></td>
                <td bgcolor="rosybrown"><?php $pro->precio = $mostrar['precio']; echo $pro->precio;?></td>
                <td bgcolor="rosybrown"><a href="modifP.php?id=<?php echo $mostrar['id'] ?>" >Modificar</a></td>                          
            </tr>
          <?php }?>
    </table> 
    </div>          
            </div>
        </div>
    </div>
</body>
</html>